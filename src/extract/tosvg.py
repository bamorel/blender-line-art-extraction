import bpy
import xml.etree.cElementTree as ET
import xml.dom.minidom

from extract.datastruct import ContextData, StrokeInfo, ObjectData, UVCoords, FrameInterval
from extract import utils
from mathutils import Vector

def get_uvcoords_list(context : ContextData, stroke : bpy.types.GPencilStroke, name : str) -> list[list[UVCoords]]:
    """Returns a list of size one or two (if the stroke is an intersection), this list contains all uvcoords of the given
    stroke"""
    if name.split()[0] == 'I':
        name = name.removeprefix('I ')
        names = [name.split(' / ')[0]]
        names.append(name.split(' / ')[1])
    else : names = [name]

    uv_coords = []
    for name in names:
        uv_coords.append(utils.uvcoords.get_uv_coords_from_stroke(stroke, context.line_art, context.table_objects[name]))
    
    return uv_coords


def get_frame_info(context : ContextData) -> list[StrokeInfo]:
    """Returns the StrokeInfo of each stroke in the current frame"""
    # store each object name encounter in this frame
    groups : set = set()
    ret = []
    for stroke in context.get_active_frame_strokes():
        name = utils.groups.get_stroke_object_name(0, context.line_art.vertex_groups, stroke)
        if name == None: 
            best_name, second_name = utils.mesh.find_intersection_triangles(context.table_objects, utils.transforms.apply_world_transform(context.line_art, stroke.points[0].co))
            if second_name == None:
                name = best_name
            else :
                name = "I " + best_name + " / " + second_name
                # one group per pair
                if name not in groups:
                    name = "I " + second_name + " / " + best_name
        if name != None and name not in groups:
            groups.add(name)

        projections = utils.transforms.stroke_to_projection_list(stroke, context)
        if projections[-1] == projections[-2]:
            projections = [projections[:-1]]
        if not utils.svg.is_valid_polyline(projections, context.scene) : continue

        uv_coords = get_uvcoords_list(context, stroke, name)
        ret.append(StrokeInfo(name, projections, uv_coords, intersection=len(uv_coords) == 2))
    return ret


def create_polyline_element(parent : ET.Element, scene : bpy.types.Scene, stroke_info : StrokeInfo, start : int = 0, end : int = None, 
                            appear : bool = None, disappear : bool = None):
    """Creates a polyline element.
    
    'start', 'end', 'appear' and 'disappear' are used for create a portion of the given stroke"""
    if appear == None : appear = stroke_info.appear
    if disappear == None : disappear = stroke_info.disappear

    size = (end + 1 if end != None else stroke_info.size) - start 
    polyline = ET.SubElement(parent, "polyline")
    polyline.set("points", utils.svg.get_polyline_from_projections(stroke_info.projections[start : end + 1 if end != None else stroke_info.size], scene))
    polyline.set("size", str(size))
    polyline.set("crease", str(1 if stroke_info.crease else 0))
    polyline.set("contour", str(1 if stroke_info.contour else 0))
    polyline.set("intersection", str(1 if stroke_info.intersection else 0))
    polyline.set("appears", str(1 if appear else 0))
    polyline.set("disappears", str(1 if disappear else 0))
    depth = ET.SubElement(polyline, "depth")
    depth.text = utils.svg.get_depth_from_projections(stroke_info.projections[start : end + 1 if end != None else stroke_info.size])

    if stroke_info.get_transforms() == None and stroke_info.qi_list == None : return 
    next_frame = ET.SubElement(polyline, "next-frame")

    if stroke_info.get_transforms() != None:
        if not stroke_info.intersection:
            transform = ET.SubElement(next_frame, "transform")
            transform.text = utils.svg.get_transform_from_list(stroke_info.transform[start : end + 1 if end != None else stroke_info.size])
        else :
            for i in range(len(stroke_info.get_transforms())):
                transform = ET.SubElement(next_frame, "transform")
                current_transform = stroke_info.intersection_data.transforms[i]
                transform.text = utils.svg.get_transform_from_list(current_transform[start : end + 1 if end != None else stroke_info.size])

    if stroke_info.qi_list != None :
        qi = ET.SubElement(next_frame, "qi")
        qi.text = utils.svg.get_qi_from_list(stroke_info.qi_list[start : end + 1 if end != None else stroke_info.size])



def create_dict_element(parent : ET.Element, attrib : str) -> dict[str, ET.Element]:
    """Returns a dictionary of each child element that has the 'attrib'.
    
    Dict [attrib value : element]"""
    dict_element = {}
    elements = parent.findall("./g[@" + attrib + "]")
    for el in elements:
        dict_element[el.get(attrib)] = el
    return dict_element


def add_polyline_to_group(dict_element : dict[str, ET.Element], parent : ET.Element, context : ContextData,
                            stroke_info : StrokeInfo, start : int = 0, end : int = None, appear : bool = None, disappear : bool = None):
    """Adds a polyline element to a group element.
    
    'start', 'end', 'appear' and 'disappear' are used for create a portion of the given stroke"""
    appear = appear if appear != None else stroke_info.appear
    disappear = disappear if disappear != None else stroke_info.disappear

    group_name = utils.svg.get_group_name(stroke_info, appear, disappear)
    element = utils.svg.get_element(group_name, dict_element, parent)

    create_polyline_element(element, context.scene, stroke_info, start, end, appear, disappear)

def compute_stroke_segmentation_of_frame(frame : ET.Element, info : StrokeInfo, context : ContextData, dict_element : dict[str, ET.Element]) -> int :
    """Computes stroke elements from the given StrokeInfo.
    
    Segments the original stroke based on the disappearing polylines"""

    name = info.get_source()
    source = context.table_objects[name]
    qi_list = utils.strokeinfo.get_list_qi(source, info.get_uv_coords(), context.camera, context.deps_graph)
    info.qi_list = qi_list

    nb_strokes = 0
    index = 0
    first = 0

    for inter, index in utils.strokeinfo.iterate_through_qi_list(qi_list, lambda x : x != 0):
        inter = inter - 1 if inter != 0 else 0
        index += 1

        if first < inter:
            add_polyline_to_group(dict_element, frame, context, info, first, inter)
            nb_strokes += 1
    
        add_polyline_to_group(dict_element, frame, context, info, inter, min(index, len(qi_list) - 1), disappear=True)

        nb_strokes += 1
        first = index

    if first < len(qi_list) - 1 :
        add_polyline_to_group(dict_element, frame, context, info, first)
        nb_strokes += 1


    return nb_strokes

def compute_strokes_of_prev_frame(prev_frame : ET.Element, dict_element : dict[str, ET.Element], context : ContextData, strokes_infos : list[StrokeInfo]) -> int:
    """Computes strokes of the previous frame, based on the current frame.
    """
    nb_strokes = 0
    for info  in strokes_infos :
        if info.intersection : 
            add_polyline_to_group(dict_element, prev_frame, context, info)
            nb_strokes += 1
            continue
        nb_strokes += compute_stroke_segmentation_of_frame(prev_frame, info, context, dict_element)

    return nb_strokes


def compute_appearing_stroke_from_next_frame(frame : ET.Element, context : ContextData, info : StrokeInfo, dict_element : dict[str, ET.Element]) -> int:
    """Coputes appearing stroke of the current frame based on the next frame"""
    name = info.get_source()
    uv_coords = info.get_uv_coords()
    next_projections = info.projections
    source = context.table_objects[name]
    qi_list = utils.strokeinfo.get_list_qi(source, uv_coords, context.camera, context.deps_graph)

    nb_strokes = 0

    for begin, end in utils.strokeinfo.iterate_through_qi_list(qi_list, lambda x : x != 0):
        begin = begin - 1 if begin != 0 else 0
        end += 1

        end = min(end + 1, len(qi_list) - 1)
        world_coords = utils.uvcoords.uv_coords_list_to_world_list(source, uv_coords[begin : end + 1])
        projections = utils.transforms.get_projection_list(context.scene, context.camera, world_coords)
        transform = utils.transforms.get_transform_vectors(projections, next_projections[begin : end + 1])
        new_stroke = StrokeInfo(name, projections, [uv_coords[begin : end + 1]], qi_list[begin : end + 1], transform=transform, crease=info.crease, appear=True)
            
        add_polyline_to_group(dict_element, frame, context, new_stroke)
        nb_strokes += 1
    #end for
    return nb_strokes

def compute_appearing_strokes_from_next_frame(frame_element : ET.Element, dict_element : dict[str, ET.Element], context : ContextData, strokes_infos : list[StrokeInfo]) -> int:
    """Computes appearing strokes of the current frame based on its next frame"""
    nb_strokes = 0
    for info in strokes_infos:
        if info.intersection : continue
        nb_strokes += compute_appearing_stroke_from_next_frame(frame_element, context, info, dict_element)
    return nb_strokes

def compute_intermediate_frame(prev_infos : list[StrokeInfo], current_infos : list[StrokeInfo], context : ContextData, parent_element : ET.Element,
                                interval : FrameInterval, use_appearing_strokes : bool):
    """Creates the previous frame element and its children"""
    previous_frame = max(interval.begin, interval.current - interval.step)

    frame_element = utils.svg.get_frame(parent_element, previous_frame, interval)
    nb_strokes = int(frame_element.get("size", "0"))
    dict_element = create_dict_element(frame_element, "name")

    nb_strokes += compute_strokes_of_prev_frame(frame_element, dict_element, context, prev_infos)

    if use_appearing_strokes: 
        context.scene.frame_set(previous_frame)
        context.deps_graph.update()
        nb_strokes += compute_appearing_strokes_from_next_frame(frame_element, dict_element, context, current_infos)
    
    frame_element.set("size", str(nb_strokes))


def compute_last_frame(infos : list[StrokeInfo], parent_element : ET.Element, interval : FrameInterval, context : ContextData):
    """Creates frame element and its children for the last frame"""
    frame = utils.svg.get_frame(parent_element, interval.end, interval)
    dict_element = create_dict_element(frame, "name")

    for info in infos:
        name = utils.svg.get_group_name(info)
        element = utils.svg.get_element(name, dict_element, frame)
        create_polyline_element(element, context.scene, info)

    size = int(frame.get("size", "0")) + len(infos)
    frame.set("size", str(size))



                
def compute_all_frames(context : ContextData, interval : FrameInterval, parent_element : ET.Element, use_appearing_strokes : bool,
                         check_crease : bool, contour : bool = False, crease : bool = False):
    """Creates frame elements and their children for all frames.
    
    In the current frame, the qi of the previous frame strokes is computed.
    So the previous frame element is created with strokes that disappears, are contour and / or crease.

    For each current stroke, their qi in the previous frame is computed.
    So appearing strokes is added to the previous frame element."""

    base_frame = context.scene.frame_current

    for i in interval.iterate_through_frames():

        context.scene.frame_set(i)
        context.deps_graph.update()
        for obj in context.table_objects.values():
            obj.reset_bvh_tree()

        current_infos = get_frame_info(context)
        utils.strokeinfo.set_list_is_crease(current_infos, context) if check_crease else utils.strokeinfo.set_list_crease(current_infos, crease)
        utils.strokeinfo.set_list_contour(current_infos, contour)
        
        if i != interval.begin:
            interval.set_current(i)
            utils.strokeinfo.set_list_transforms(prev_infos, context)
            compute_intermediate_frame(prev_infos, current_infos, context, parent_element, interval, use_appearing_strokes)
            
      
        if i == interval.end :
            compute_last_frame(current_infos, parent_element, interval, context)

        prev_infos = current_infos

    # end for
    context.scene.frame_set(base_frame)
    context.deps_graph.update()


def to_svg (gp : bpy.types.Object, filepath : str, camera : bpy.types.Camera, scene : bpy.types.Scene, interval : FrameInterval,
            use_appearing_strokes : bool, use_intersection : bool) -> None:
    """Creates a svg with the following structure :
    * svg
    * --lineart
    * ----frame
    * ------group name
    * --------stroke
    * ----------depth
    * ----------next-frame
    * ------------transform
    * ------------qi
    """
    svg = ET.Element("svg", utils.svg.get_svg_attribs(scene))
    deps_graph = bpy.context.evaluated_depsgraph_get()
    line_art = bpy.data.objects[gp.name]



    # group transfer
    utils.lineart.use_group_transfer(line_art)
    objects = utils.groups.get_list_of_mesh(bpy.data.objects)
    for obj in objects:
        utils.groups.create_group(obj, line_art)
    deps_graph.update()

    # use objects with modifiers
    objects = utils.groups.get_list_of_mesh(deps_graph.objects)

    deps_graph.update()
    line_art = deps_graph.objects[gp.name]
    table_objects = {obj.name : ObjectData(obj, deps_graph, utils.lineart.get_crease_threshold(line_art)) for obj in objects}
    context = ContextData(camera, scene, deps_graph, line_art, table_objects)

    g = ET.SubElement(svg, "g", {"name" : line_art.name})

    # two loop in order to avoid computing contours

    # use only creases, i.e stroke that is crease and not contour
    utils.lineart.unselect_all(line_art)
    utils.lineart.use_crease(line_art, True)
    compute_all_frames(context, interval, g, use_appearing_strokes, False, False, True)
    print("creases -- OK")

    # use contours, i.e stroke that is contour and could be crease
    utils.lineart.use_crease(line_art, False)
    utils.lineart.use_contour(line_art, True)
    compute_all_frames(context, interval, g, use_appearing_strokes, True, True, False)
    print("contour -- OK")
        
    # use only intersections
    utils.lineart.use_contour(line_art, False)
    if use_intersection :
        utils.lineart.use_intersections(line_art, True)
        compute_all_frames(context, interval, g, use_appearing_strokes, False, False, False)
        print("intersections -- OK")

    # deleting all added groups
    objects = utils.groups.get_list_of_mesh(bpy.data.objects)
    objects.append(bpy.data.objects[gp.name])
    for obj in objects:
        utils.groups.delete_groups(obj)

    # export
    dom = xml.dom.minidom.parseString(ET.tostring(svg))
    xml_string = dom.toprettyxml()
    part1, part2 = xml_string.split('?>')
    
    with open(filepath, 'w') as out:
        out.write(part1 + 'encoding=\"UTF-8\"?>\n' + part2)
        out.close()