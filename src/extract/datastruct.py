import bpy
import bmesh
import math

from bmesh.types import BMesh
from mathutils.bvhtree import BVHTree
from mathutils import Vector

#..........................................................
#  O B J E C T  D A T A
#..........................................................

class ObjectData :
    """
    Used for computing on any mesh object
    """
    def __init__(self, obj : bpy.types.Object, deps_graph : bpy.types.Depsgraph, angle : int):
        self.obj : bpy.types.Object = obj
        self.mesh : bpy.types.Mesh = obj.data

        bm = bmesh.new()
        bm.from_object(obj, deps_graph)
        ObjectData._evaluate_bmesh(bm)

        self.bm : BMesh = bm

        self._set_edges_tag(angle)

        # for ray casting
        self.triangles : list[bmesh.types.BMLoop] = bm.calc_loop_triangles()
        self.bvh_tree : BVHTree = ObjectData._construct_bvhtree_triangles(self)

        # angle for detecting creases
        self.crease : int = angle

    def reset_bvh_tree(self) -> None:
        self.bvh_tree = ObjectData._construct_bvhtree_triangles(self)

    # use for creases
    def _set_edges_tag(self, angle_deg : float) -> None:
        for edge in self.bm.edges:
            try:
                angle_calc = 180 - math.degrees(edge.calc_face_angle(False))
                angle_calc = round(angle_calc, 4)
                angle_deg = round(angle_deg, 4)
                edge.tag = angle_calc <= angle_deg
            except:
                print("EXCEPT -- edge.calc_face_angle() not feasible")
                continue

    def _evaluate_bmesh(bm : BMesh):
        bm.verts.ensure_lookup_table()
        bm.edges.ensure_lookup_table()
        bm.faces.ensure_lookup_table()

    # in world space
    def _construct_bvhtree_triangles(self) -> BVHTree:
        mesh = self.obj.data
        matrix = self.obj.matrix_world

        loops = self.triangles

        vertices = [matrix @ v.co for v in mesh.vertices]

        polygons = [(loop[0].vert.index, loop[1].vert.index, loop[2].vert.index) for loop in loops]

        tree = BVHTree.FromPolygons(vertices, polygons, all_triangles = True)
        return tree


#..........................................................
#  C O N T E X T  D A T A
#..........................................................

# data mainly used
class ContextData:
    """Stores all objects mainly used

    * camera : the current camera
    * scene : the current scene
    * deps_graph : the dependancy graph of the scene
    * line_art : the Line Art exported
    * table_objects : dictionary -- object name -> Object Data 
    """

    def __init__(self, camera : bpy.types.Camera, scene : bpy.types.Scene, deps_graph : bpy.types.Depsgraph, line_art : bpy.types.Object, table_objects : dict[str, ObjectData]):
        self.camera : bpy.types.Camera = camera
        self.scene : bpy.types.Scene = scene
        self.deps_graph : bpy.types.Depsgraph= deps_graph
        self.line_art : bpy.types.Object= line_art
        self.table_objects : dict[str, ObjectData]= table_objects

    def get_active_frame_strokes(self) -> bpy.types.GPencilStrokes:
        return self.line_art.data.layers.active.active_frame.strokes

    def get_resolution(self) -> tuple[int]:
        render = self.render
        return (render.resolution_x, render.resolution_y)

#..........................................................
#  I N T E R S E C T I O N  D A T A
#..........................................................

class IntersectionData:
    """Used for storing strokes that are intersections"""
    def __init__(self, sources : tuple[str, str], uv_coords : tuple[list['UVCoords'], list['UVCoords']], transforms : tuple[list[Vector], list[Vector]]):
        self.sources : tuple [str, str] = sources
        self.uv_coords : tuple [list['UVCoords'], list['UVCoords']] = uv_coords
        self.transforms : tuple[list[Vector], list[Vector]] = transforms

    def get_source(self):
        if self.sources == None : return None
        return self.sources[0]

    def get_uv_coords(self):
        if self.uv_coords == None : return None
        return self.uv_coords[0]
    
    def get_transform(self):
        if self.transforms == None : return None
        return self.transforms

#..........................................................
#  S T R O K E  I N F O
#..........................................................

class StrokeInfo:
    """Stores all data needed for creating a SVG Stroke element"""

    def __init__(self, source : str, projections : list[Vector], uv_coords_list : list[list['UVCoords']] = None, qi_list : list[int] = None, transform : list[Vector] = None,
                    crease : bool = False, contour : bool = False, intersection : bool = False, appear : bool = False, disappear : bool = False):
        # source name
        self.source : str = source
        
        # in screen space
        self.projections : list[Vector] = projections
        self.size : int = len(projections)

        # qi of this stroke in next keyframe
        self.qi_list : list[int] = qi_list

        # transform to apply for getting the next keyframe position
        self.transform : list[Vector] = transform

        self.crease : bool = crease
        self.contour : bool = contour
        self.intersection : bool = intersection
        self.appear : bool = appear
        self.disappear : bool = disappear


        self.uv_coords : list['UVCoords'] = uv_coords_list[0]

        if intersection:
            split = source.removeprefix("I ")
            split = split.split(" / ")
            sources = (split[0], split[1])
            self.intersection_data = IntersectionData(sources, uv_coords_list, None)
        else :
            self.intersection_data = None

    def get_source(self):
        if not self.intersection:
            return self.source
        else :
            return self.intersection_data.get_source()

    def get_uv_coords(self):
        if not self.intersection:
            return self.uv_coords
        else :
            return self.intersection_data.get_uv_coords()

    def get_transforms(self):
        if not self.intersection:
            return self.transform
        else :
            return self.intersection_data.get_transform()
#..........................................................
#  U V  C O O R D S
#..........................................................

class UVCoords:
    """Stores UVCoords of a point on a given triangle index"""
    def __init__(self, index : int, u : int, v : int):

        # index of a triangle
        self.index : int = index        

        self.u : int = u
        self.v : int = v
        self.w : int = 1 - u - v

        # print("index = {}, u = {}, v = {}, w = {}".format(index, round(u, 4), round(v, 4), round(self.w, 4)))

#..........................................................
#  F R A M E  I N T E R V A L
#..........................................................
    
class FrameInterval : 
    """Stores bounds and step of the frame intervals and the current frame"""
    def __init__(self, begin : int, end : int, step : int, current : int = None):
        self.begin = begin
        self.end = end
        self.step = step
        self.current = current

    def set_current(self, value : int) :
        self.current = value
   
    def iterate_through_frames(self):
        """Returns a generator that iterate through all frames in the interval"""
        i = self.begin
        yield i
        while i < self.end:
            i += self.step
            if i > self.end : 
                i = self.end
            yield i

            


    