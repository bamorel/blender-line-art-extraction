import importlib
import sys


# applying any changement in modules files
def _reload(name : str) -> bool:
    if name in sys.modules.keys():
        module = sys.modules[name]
        importlib.reload(module)
        return True
    return False

PARENT = "extract."

if not _reload(PARENT + "datastruct"):
    import extract.datastruct as datastruct

if not _reload(PARENT + "panel"):
    import extract.panel as panel

if not _reload(PARENT + "utils"):
    import extract.utils as utils

if not _reload(PARENT + "tosvg"):
    import extract.tosvg as tosvg