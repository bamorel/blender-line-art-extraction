import bpy
import time as tm

from extract import utils
from extract import tosvg
from extract.datastruct import FrameInterval

#..........................................................
#   P R I N T
#..........................................................

class Print: 

    @staticmethod
    def showMessageBox(message : str = "" , title : str = "Message Box", icon : str = 'INFO'):
        """Open a message box with the given message"""

        def draw(self, context):
            self.layout.label(text=message)

        bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

    @staticmethod     
    def print_error(message : str) -> None:
        print("Error : " + message)

    @staticmethod
    def print_operator_error(message : str) -> str:
        """Output an error message"""
        Print.print_error(message)
        Print.showMessageBox(message, title="Error", icon='ERROR')
        return {"CANCELLED"}


#..........................................................
#   O P E R A T O R
#..........................................................




class GPExportToSVG(bpy.types.Operator):
    bl_idname = 'gpencil.export_to_svg'
    bl_label = 'Export to SVG'

    def execute(self, context : bpy.types.Context):

        filepath = context.scene.export.fileSVG


        gp = utils.view.get_only_one_selected_gpencil()
        camera = utils.view.get_camera(context.scene)
        interval = FrameInterval(context.scene.export.frame_begin, context.scene.export.frame_end, context.scene.export.step)

        if gp == None : return Print.print_operator_error("(Only) one GPENCIL need to be selected")
        if camera == None : return Print.print_operator_error("One CAMERA is needed")

        start = tm.time()
        print("Starting extraction")
        tosvg.to_svg(gp, filepath, camera, context.scene, interval, context.scene.export.appearing_strokes, context.scene.export.use_intersection)
        end = tm.time()
        print("Duration : " + str(end - start) + "s")
        Print.showMessageBox("Done", "Information")
        return {"FINISHED"}



class OpenBrowserSVG(bpy.types.Operator):
    bl_idname = "open.browser_svg"
    bl_label = "Select SVG File"

    filepath : bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context : bpy.types.Context): 
        context.scene.export.fileSVG = self.filepath
        return {"FINISHED"}

    def invoke(self, context : bpy.types.Context, event : bpy.types.Event):
        context.window_manager.fileselect_add(self)
        return {"RUNNING_MODAL"}




#..............................................
#   P R O P E R T Y
#..............................................


class ExportProperties(bpy.types.PropertyGroup):
    fileSVG : bpy.props.StringProperty(name = "filePath_svg", default = "")
    frame_begin : bpy.props.IntProperty(name = "frame_begin", default = 1, min=1)
    frame_end : bpy.props.IntProperty(name = "frame_end", default = 1, min=1)
    step : bpy.props.IntProperty(name = "step", default = 1, min = 1)
    appearing_strokes : bpy.props.BoolProperty(name = "appearing_strokes", default = True)
    use_intersection : bpy.props.BoolProperty(name = "use intersections", default = False)

    



#.................................................
#   P A N E L
#.................................................



class ConvertToSVG(bpy.types.Panel):
    bl_idname = "GPENCIL_PT_export_svg"
    bl_label = "Export to SVG"
    bl_space_type = "VIEW_3D"
    bl_region_type = 'UI'
    bl_category = "Line Art"

    def draw(self, context : bpy.types.Context):
        export = context.scene.export

        self.layout.operator("open.browser_svg", icon="FILE_FOLDER")
        self.layout.prop(export, "fileSVG", text="File")
        box = self.layout.box()
        box.label(text="Frames")

        row = box.row()
        row.prop(export, "frame_begin", text="First")
        row.prop(export, "frame_end", text="Last")

        box.row().prop(export, "step", text = "Step")
        
        self.layout.prop(export, "appearing_strokes", text = "Appearing strokes")
        self.layout.prop(export, "use_intersection", text = "Intersections")

        self.layout.operator("gpencil.export_to_svg")




#.................................................
#   M A I N
#.................................................


classes = (
    OpenBrowserSVG,
    ExportProperties,
    GPExportToSVG,
    ConvertToSVG
)

def register():
    for c in classes:
        bpy.utils.register_class(c)

    bpy.types.Scene.export = bpy.props.PointerProperty(type=ExportProperties)    

def unregister():
    del bpy.types.Scene.export

    for c in reversed(classes):
        bpy.utils.unregister_class(c)