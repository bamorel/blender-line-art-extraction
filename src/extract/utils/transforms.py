import bpy

from bpy_extras.object_utils import world_to_camera_view
from mathutils import Vector
import extract.datastruct as datastruct
import extract.utils as utils


def stroke_to_projection_list(stroke : bpy.types.GPencilStroke, context : datastruct.ContextData) -> list[Vector]:
    """Returns projections of each point of the stroke, fit to the resolution"""
    coords = [apply_world_transform(context.line_art, point.co) for point in stroke.points]
    return get_projection_list(context.scene, context.camera, coords)

def get_projection_list(scene : bpy.types.Scene, camera : bpy.types.Camera, coords : list[Vector]) -> list[Vector]:
    """Returns projections of given coords, fit to the resolution"""
    res_x, res_y = utils.view.get_resolution(scene)
    projections = [world_to_camera_view(scene, camera, co) for co in coords]
    for proj in projections:
        proj[1] = res_y - proj[1] * res_y
        proj[0] = proj[0] * res_x
    return projections

def apply_world_transform(obj_source : bpy.types.Object, point : Vector) -> Vector:
    """Returns the point in the world space"""
    return obj_source.matrix_world @ point

def get_transform_vectors(current_projections : list[Vector], next_projections : list[Vector]) -> list[Vector]:
    """Returns a list of transformation vector from current_projections to next_projections"""
    ret = []
    for i in range(len(current_projections)):
        vec = next_projections[i] - current_projections[i]
        for j in range(2):
            vec[j] = round(vec[j], 3)
        ret.append(vec)
    return ret