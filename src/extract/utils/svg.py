import bpy

from mathutils import Vector
import xml.etree.cElementTree as ET

import extract.utils as utils
import extract.datastruct as datastruct


def is_valid_polyline(projections : list[Vector], scene : bpy.types.Scene) -> bool:
    """Returns 'True' if projections contains at least 2 points
    and all of its points are in the camera view.

    Returns 'False' otherwise."""
    res_x, res_y = utils.view.get_resolution(scene)
    if len(projections) < 2: return False
    for v in projections :
        if v[0] > res_x or v[0] < 0 or v[1] > res_y or v[1] < 0:
            return False
    return True

def get_polyline_from_projections(projections : list[Vector], scene : bpy.types.Scene) -> str:
    """Returns a string corresponding to a 'points' attribute of a polyline element"""
    res_x, res_y = utils.view.get_resolution(scene)
    off_x = - res_x / 2
    off_y = - res_y / 2
    ret = ""
    for v in projections:
        ret += str(v[0] + off_x) + " " + str(v[1] + off_y) + " "
    return ret

def get_depth_from_projections(projections : list[Vector]) -> str:
    """Returns a string corresponding to a 'depht' element"""
    depth = ""
    for v in projections:
        depth += str(v[2]) + " "
    return depth

def get_transform_from_list(vectors : list[Vector]) -> str:
    """Returns a string corresponding to a 'transform' element"""
    ret = ""
    for v in vectors:
        ret += str(v[0]) + " " + str(v[1]) +  " "
    return ret
        
def get_qi_from_list(qi_list : list[int]) -> str:
    """Returns a string corresponding to a 'qi' element"""
    return " ".join(str(x) for x in qi_list)

def get_svg_attribs(scene : bpy.types.Scene) -> dict[str, str]:
    """Returns a dictionary of the svg attributes"""
    res_x, res_y = utils.view.get_resolution(scene)
    off_x = res_x / 2
    off_y = res_y / 2
    attrib = {
        "x" : "0px",
        "y" : "0px",
        "width" : str(res_x),
        "height" : str(res_y),
        "viewBox" : str(-off_x) + " " + str(-off_y) + " " + str(res_x) + " " + str(res_y),
        "xmlns" : "http://www.w3.org/2000/svg"
    }
    return attrib
    
def get_frame_attrib(first_frame : int, last_frame : int, i : int) -> dict[str, str]:
    """Returns a dictionary of the frame attributes"""
    opacity = 1 if (i == first_frame or i == last_frame) else 0.4

    attrib = {
            "frame" : str(i),
            "fill" : "none",
            "stroke-width" : "3",
            "stroke-linecap" : "round",
            "stroke-opacity" : str(opacity)
        }
    return attrib

def get_group_name(info : datastruct.StrokeInfo, appear : bool = False, disappear : bool = False) -> str:
    """Returns the group name attached to the StrokeInfo"""
    group_name = info.source
    if info.intersection:
        return group_name
    elif disappear :
        group_name += " disappear"
    elif appear:
        group_name += " other"
    elif info.contour and not info.crease:
        group_name += " contour"
    else : 
        group_name += " default"
    return group_name

def get_frame(parent_element : ET.Element, frame_nb : int, frame_info : datastruct.FrameInterval) -> ET.Element:
    """Returns the child frame element n° 'frame_nb'.
    
    Otherwise returns a new frame element with attributes set with 'frame_info'"""

    frame = parent_element.find("./g[@frame='" + str(frame_nb) + "']")
    if frame == None: 
        attrib = get_frame_attrib(frame_info.begin, frame_info.end, frame_nb)
        frame = ET.SubElement(parent_element, "g", attrib)
    return frame

def get_element(name : str, dict_element : dict[str, ET.Element], parent : ET.Element) -> ET.Element:
    """Returns the group element that correspond to the given name"""
    if dict_element.get(name) == None:
        element = ET.SubElement(parent, "g", {"name" : name})
        dict_element[name] = element
        names = name.split(" ")
        if names[-1] == "contour" :
            element.set("stroke", "blue")
        elif names[-1] == "disappear" :
            element.set("stroke", "red")
        elif names[-1] == "default" :
            element.set("stroke", "black")
        elif names[-1] == "other" : 
            element.set("stroke", "green")
            element.set("stroke-dasharray", "8")
        else :
            element.set("stroke", "purple")
    return dict_element[name]