import bpy


def get_only_one_object_with_modifier(type : str, list : list[bpy.types.Object]) -> bpy.types.Object | None:
    """Returns one object from the list of type 'type'.

    If more than one object of that type is in the list returns None.

    If none object of this type is in the list, returns None."""

    obj = None
    deps_graph = bpy.context.evaluated_depsgraph_get()
    for selected in list:
        name = selected.name
        if selected.type == type:
            if obj == None: obj = deps_graph.objects[name]
            else : return None
    return obj

def get_only_one_selected_object(type : str) -> bpy.types.Object:
    """Returns one object selected of type 'type'
    
    If more than one object of that type is selected, returns None
    
    If none object of this type is selected, returns None"""
    return get_only_one_object_with_modifier(type, bpy.context.selected_objects)


def get_only_one_selected_gpencil() -> bpy.types.GreasePencil:
    """Returns the only one grease pencil selected"""
    return get_only_one_selected_object('GPENCIL')

def get_camera(scene : bpy.types.Scene) -> bpy.types.Camera:
    """Returns the only one camera selected"""
    return get_only_one_object_with_modifier('CAMERA', scene.objects)

def get_resolution(scene : bpy.types.Scene) -> tuple[int]:
    """Returns the resolution used"""
    render = scene.render
    return (render.resolution_x, render.resolution_y)







        


