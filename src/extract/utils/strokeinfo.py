import bpy

from typing import Callable
from mathutils import Vector

import extract.datastruct as datastruct
import extract.utils as utils

def is_crease(info : datastruct.StrokeInfo, context : datastruct.ContextData) -> bool:
    """Returns if the StrokeInfo store a creased stroke or not"""
    edge = utils.mesh.get_incident_edge(context.table_objects[info.source], info.uv_coords[0], info.uv_coords[1])
    if type(edge) == bool:
        return edge
    else :
        return edge.tag

def set_list_contour(infos : list[datastruct.StrokeInfo], contour : bool):
    """Sets all StrokeInfo.contour to the given value"""
    for info in infos:
        info.contour = contour

def set_list_crease(infos : list[datastruct.StrokeInfo], crease : bool):
    """Sets all StrokeInfo.crease to the given value"""
    for info in infos:
        info.crease = crease

def set_list_is_crease(infos : list[datastruct.StrokeInfo], context : datastruct.ContextData):
    """Computes and sets all StrokeInfo.crease"""
    for info in infos:
        info.crease = is_crease(info, context)

def set_list_transforms(infos : list[datastruct.StrokeInfo], context : datastruct.ContextData):
    """Sets transforms of all StrokeInfo, from their projections to the current projection"""
    for info in infos:
        transforms = info.get_transforms()
        if transforms == None :
            if not info.intersection:
                info.transform = utils.uvcoords.get_transform_vector_from_uv(context.table_objects[info.get_source()], info.get_uv_coords(), context, info.projections)
            else :
                for i in range(2):
                    transforms = []
                    source = context.table_objects[info.intersection_data.sources[i]]
                    uv_coords = info.intersection_data.uv_coords[i]
                    transforms.append(utils.uvcoords.get_transform_vector_from_uv(source, uv_coords, context, info.projections))
                    info.intersection_data.transforms = transforms

def iterate_through_qi_list(qi_list : list[int], predicate : Callable[[int], bool]):
    """Creates a generator that returns each intervals of values from 'qi_list' that satisfyed the predicate"""
    index = 0
    while index < len(qi_list):
        if not predicate(qi_list[index]) :
            index += 1
            continue

        start = index
        while index < len(qi_list):
            if not predicate(qi_list[index]) : break
            index += 1

        yield (start, index - 1)

def get_list_qi(object : datastruct.ObjectData, uv_coords : list[datastruct.UVCoords], camera : bpy.types.Object, dp : bpy.types.Depsgraph) -> list[int]:
    """Returns the qi of each uvcoords in the current frame"""
    world_coords = utils.uvcoords.uv_coords_list_to_world_list(object, uv_coords)
    ret = []
    for coord in world_coords:
        ret.append(get_point_qi(coord, camera.location, dp))
    return ret

def get_point_qi(point_pos : Vector, camera_pos : Vector, dp : bpy.types.Depsgraph) -> int:
    """Returns the qi of a given point in the current frame"""
    direction = (camera_pos - point_pos).normalized()
    location = point_pos

    epsilon = direction * 1e-6  
    count = 0
    result = True

    MAX = 20

    while result and count < MAX:  
        result, location, normal, index, object, matrix = bpy.context.scene.ray_cast(dp, location + epsilon, direction)
        if result : count += 1
    return count