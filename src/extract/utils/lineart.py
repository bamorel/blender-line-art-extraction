import bpy
import math


def use_group_transfer(line_art : bpy.types.Object):
    """Initialize the Line Art modifier, allows used of groups"""
    line_art.grease_pencil_modifiers['Line Art'].use_output_vertex_group_match_by_name = True

def use_contour(line_art : bpy.types.Object, bool : bool = True):
    """Set use of contour to 'bool'"""
    line_art.grease_pencil_modifiers['Line Art'].use_contour = bool

def use_crease(line_art : bpy.types.Object, bool : bool = True, threshold : int = None):
    """Set use of crease to 'bool' with the given threshold (keep the default value if not given)"""
    line_art.grease_pencil_modifiers['Line Art'].use_crease = bool
    if threshold != None and threshold >= 0 and threshold <= 180 :
        rad = math.radians(threshold)
        line_art.grease_pencil_modifiers['Line Art'].crease_threshold = rad

def use_intersections(line_art : bpy.types.Object, bool : bool = True):
    """Set use of intersections to 'bool'"""
    line_art.grease_pencil_modifiers['Line Art'].use_intersection = bool

def unselect_all(line_art : bpy.types.Object):
    """Set all stroke sources to unused"""
    modifier = line_art.grease_pencil_modifiers['Line Art']
    modifier.use_contour = False
    modifier.use_loose = False
    modifier.use_material = False
    modifier.use_edge_mark = False
    modifier.use_intersection = False
    modifier.use_crease = False 
    modifier.use_overlap_edge_type_support = False

def get_crease_threshold(line_art : bpy.types.Object):
    """Return the crease threshold"""
    return math.degrees(line_art.grease_pencil_modifiers['Line Art'].crease_threshold)