import bpy
from mathutils import Vector

import extract.datastruct as datastruct
import extract.utils as utils



def get_barycentric_coords(pos : Vector, triangle_index : int, object_data : datastruct.ObjectData) -> tuple[float]:
    """Returns the barycentric coords of the given pos, in the given triangle"""
    loops = object_data.triangles
    a, b, c = [utils.transforms.apply_world_transform(object_data.obj, loops[triangle_index][i].vert.co) for i in range(3)]
    p = pos
    return compute_barycentric_coords(p, a, b, c)

def compute_barycentric_coords(p : Vector, a : Vector, b : Vector, c : Vector) -> tuple[float]:
    """Returns the UV of the point p, in the triangle a - b - c"""
    area = compute_triangle_area(a, b, c)
    u = compute_triangle_area(c, a, p)
    if u != 0:
        u /= area

    v = compute_triangle_area(a, p, b)
    if v != 0:
        v /= area

    return u, v

def compute_triangle_area(a : Vector, b : Vector, c : Vector) -> float:
    """Returns the area of the triangle constructed by the given points"""
    cross = (b - a).cross(c - a)
    return cross.length / 2

def from_barycentric_to_world_coords(barycentric : datastruct.UVCoords, obj_source : datastruct.ObjectData) -> Vector :
    """Returns the world coords of the given UVCoords"""
    loops = obj_source.triangles    
    a, b, c = [utils.transforms.apply_world_transform(obj_source.obj, loops[barycentric.index][i].vert.co) for i in range(3)]
    return barycentric.u * b + barycentric.v * c + barycentric.w * a

def get_transform_vector_from_uv(object: datastruct.ObjectData, uv_coords : list[datastruct.UVCoords], context : datastruct.ContextData, projections : list[Vector]) -> list[Vector]:
    """Returns transforms from 'projections' to the current frame projections"""
    world_coords = uv_coords_list_to_world_list(object, uv_coords)
    next_frame_projections = utils.transforms.get_projection_list(context.scene, context.camera, world_coords)
    return utils.transforms.get_transform_vectors(projections, next_frame_projections)
    

def uv_coords_list_to_world_list(obj_source : datastruct.ObjectData, coords : list[datastruct.UVCoords]) -> list[Vector]:
    """Returns coords in the world space"""
    return [from_barycentric_to_world_coords(co, obj_source) for co in coords]

def get_uv_coords_from_stroke(stroke : bpy.types.GPencilStroke, line_art : bpy.types.Object, obj_source : datastruct.ObjectData) -> list[datastruct.UVCoords]:
    """Returns list of UVCoords that correspond to stroke"""
    ret = []
    for point in stroke.points:
        pos = utils.transforms.apply_world_transform(line_art, point.co)
        index = utils.mesh.find_triangle_id(pos, obj_source.bvh_tree)
        u, v = utils.uvcoords.get_barycentric_coords(pos, index, obj_source)
        ret.append(datastruct.UVCoords(index, u, v))
    return ret