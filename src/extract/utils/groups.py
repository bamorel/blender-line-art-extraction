import bpy


def get_list_of_mesh(objects : list[bpy.types.Object]) -> list[bpy.types.Object]:
    """Returns only objects that are meshes"""
    return [obj for obj in objects if obj.type == 'MESH']

def delete_groups(obj : bpy.types.Object) -> None:
    """Deletes all groups previously created of the given object"""
    for group in obj.vertex_groups:
        if group.name.split("_")[0] == "Group":
            obj.vertex_groups.remove(group)

def create_group(obj : bpy.types.Object , line_art : bpy.types.GreasePencil = None) -> None:
    """Creates one group per object and link it to line_art if it is given"""
    group_name = "Group_" + obj.name
    group = obj.vertex_groups.new(name = group_name)
    group.add([0], 1, 'ADD')
    if line_art != None:
        line_art.vertex_groups.new(name = group_name)

def get_stroke_object_name(index : int , groups : bpy.types.VertexGroups, stroke : bpy.types.GPencilStroke) -> str | None:
    """Given a stroke, returns the object name attached to it"""
    k = 0
    for group in groups:
        weight = stroke.points.weight_get(vertex_group_index = k, point_index = index)
        if weight == -1 : 
            k += 1
            continue
        return group.name.split("_", 1)[1]
    return None

