import math
import extract.datastruct as datastruct
from bmesh.types import BMEdge, BMVert

from mathutils import Vector
from mathutils.bvhtree import BVHTree


def triangles_are_forming_crease(object : datastruct.ObjectData , first : datastruct.UVCoords, second : datastruct.UVCoords) -> bool:
    """Returns if the two triangles form a crease or not"""
    first_tri = object.triangles[first.index][0]
    second_tri = object.triangles[second.index][0]
    first_norm = first_tri.calc_normal()
    second_norm = second_tri.calc_normal()

    dot = round(first_norm.dot(second_norm), 6)
    angle_calc = math.acos(dot)
    angle_calc = 180 - math.degrees(angle_calc)
    return angle_calc <= object.crease

def find_triangle_id(point : Vector, bvh_tree_triangles : BVHTree) -> int:
    """Returns triangle id of the given point"""
    found_location, normal, index, distance = bvh_tree_triangles.find_nearest(point)
    return index



# refactoring needed .............................................


def get_edge_or_vertex(obj_source : datastruct.ObjectData, barycentric : datastruct.UVCoords) -> tuple[bool, BMVert | BMEdge] | None:
    """Returns if the given UVCoords is on a vertex or an edge and the corresponding object

    Otherwise returns None
    """
    loops = obj_source.triangles
    a, b, c = [loops[barycentric.index][i] for i in range(3)]

    first_vert = None
    second_vert = None
    third_vert = None
    w = round(barycentric.w, 4)
    if w == 1:
        return True, a.vert
    if w == 0:
        first_vert = b.vert
        second_vert = c.vert
        third_vert = a.vert

    u = round(barycentric.u, 4)
    if u == 1:
        return True, b.vert
    if u == 0:
        first_vert = a.vert
        second_vert = c.vert
        third_vert = b.vert

    v = round(barycentric.v, 4)
    if v == 1:
        return True, c.vert
    if v == 0:
        first_vert = a.vert
        second_vert = b.vert
        third_vert = c.vert

    if first_vert == None or second_vert == None:
        return None

    for edge in first_vert.link_edges:
        if edge.other_vert(first_vert) == second_vert:
            return False, edge

    for edge in first_vert.link_edges:
        if edge.other_vert(first_vert) == third_vert:
            return False, edge

    for edge in second_vert.link_edges:
        if edge.other_vert(second_vert) == third_vert:
            return False, edge
    
    return None


def get_incident_edge(obj_source : datastruct.ObjectData, first_coords : datastruct.UVCoords, second_coords : datastruct.UVCoords) -> BMEdge | bool:
    """Returns the shared edge of the two coords
    
    If it is not found, returns if the triangles of the coords form a crease or not"""
    ret = get_edge_or_vertex(obj_source, first_coords)
    if ret == None: return triangles_are_forming_crease(obj_source, first_coords, second_coords)
        
    first_vert, first = ret

    ret = get_edge_or_vertex(obj_source, second_coords)
    if ret == None : return triangles_are_forming_crease(obj_source, first_coords, second_coords)
    second_vert, second = ret
    


    if first_vert and second_vert:
        for edge in first.link_edges:
            if edge.other_vert(first) == second:
                return edge
                
    if not first_vert and not second_vert:
        if first == second : return first
        
    if first_vert and not second_vert:
        if second.other_vert(first) != None : return second

    if not first_vert and second_vert:
        if first.other_vert(second) != None : return first
    
    return triangles_are_forming_crease(obj_source, first_coords, second_coords)

def find_intersection_triangles(table_objects : dict[str, datastruct.ObjectData], position : Vector):
    """Returns the 2 nearest triangles of a given position, among all meshes"""

    best_distance = None
    best_name = None

    second_distance = None
    second_name = None

    for key, value in table_objects.items():
        found_location, normal, index, distance = value.bvh_tree.find_nearest(position)
        if best_distance == None :
            best_distance = distance
            best_name = key

        elif round(best_distance, 7) > round(distance, 7) :
            second_distance = best_distance
            second_name = best_name

            best_distance = distance
            best_name = key

        elif second_distance == None :
            second_distance = distance
            second_name = key

    return (best_name, second_name)