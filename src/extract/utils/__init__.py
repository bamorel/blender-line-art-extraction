import importlib
import sys


# allows to apply any changement in modules files
def _reload(name : str) -> bool:
    if name in sys.modules.keys():
        module = sys.modules[name]
        importlib.reload(module)
        return True
    return False

PARENT = "extract.utils."

if not _reload(PARENT + "groups"):
    import extract.utils.groups as groups

if not _reload(PARENT + "lineart"):
    import extract.utils.lineart as lineart

if not _reload(PARENT + "mesh"):
    import extract.utils.mesh as mesh

if not _reload(PARENT + "strokeinfo"):
    import extract.utils.strokeinfo as strokeinfo

if not _reload(PARENT + "svg"):
    import extract.utils.svg as svg

if not _reload(PARENT + "transforms"):
    import extract.utils.transforms as transforms

if not _reload(PARENT + "uvcoords"):
    import extract.utils.uvcoords as uvcoords

if not _reload(PARENT + "view"):
    import extract.utils.view as view