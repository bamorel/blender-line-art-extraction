import importlib
import sys

import bpy, os

# another option could be to add manually the absolute path of this folder
for area in bpy.context.screen.areas:
    if area.type == 'TEXT_EDITOR':
        for space in area.spaces:
            if space.type == 'TEXT_EDITOR':
                # needed for importing modules
                scripts_folder = os.path.dirname(space.text.filepath)

# scripts_folder = "..."

if not scripts_folder in sys.path:
        sys.path.append(scripts_folder)

if __name__ == "__main__":
    # applying any changement in modules files
    if "extract" in sys.modules.keys():
        extract = sys.modules["extract"]
        extract.panel.unregister()
        importlib.invalidate_caches()
        importlib.reload(extract)
    else : 
        import extract
        
    extract.panel.register()