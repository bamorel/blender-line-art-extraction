# Blender Line Art Extraction


## How to use it


1. Execute `extract_lineart.py` in an open blender *Text editor*.

2. An **Line Art** panel should appear on the *Viewport* sidebar region.

3. Select the output file.

4. Define the *first* / *last* frame, the *step* and if you want to add appearing strokes. 

5. Select the *Grease Pencil* object you want to export. This *Grease Pencil* need to has one *Line Art modifier* with *Source Type* setted on *Scene*.
> Otherwise, over the *Viewport* press *Shift + A* then choose ***Grease Pencil > Scene Line Art***.

6. Click on *Export to SVG* button.

---

> For applying *files* modification, just rerun the `extract_lineart.py` script inside the blender *Text editor*.

## Output

```xml
<svg>
    <g name=""> <!-- Name of the Grease Pencil used -->
        <g frame=""> <!-- Frame index -->
            <g name=""> <!-- Group name = Mesh name + Group type || Intersection -->
                <polyline points="" size="" crease="" contour="" intersection="" appears="" disappears="">  
                <!-- X Y coords of each point of the stroke, it size and booleans (0 or 1) -->
                    <depth></depth> <!-- Depth of each point -->
                    <next-frame> <!-- if a next frame exists -->
                        <transform></transform> <!-- The transformation to apply for getting the position of each point in the next frame -->
                        <qi></qi> <!-- Quantitave invisibility of each point in the next frame -->
                    </next-frame>
                </polyline>
            </g>
        </g>
    </g>
</svg>
```

| Group type                | Description                                           |
|---------------------------|-------------------------------------------------------|
| *contour*                 | Stroke that is a contour and not crease               |
| *disappear*               | Stroke that will disappear in the next frame          |
| *other*                   | Stroke that doesn't exist in the current frame        |
| *I* ***x*** */* ***y***   | Stroke that is an intersection between *x* and *y*    |
| *default*                 | Default stroke                                        |

